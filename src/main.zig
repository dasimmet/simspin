const std = @import("std");
const builtin = @import("builtin");
const spin = @import("spin");
const ZBuild = @import("ZBuild").ZBuild;
pub const rem = @import("rem");

pub const Router = @import("Router.zig");

const BODY_CAP: usize = 4096;

fn error_handler(req: spin.http.Request) spin.http.Response {
    var headers = std.http.Headers.init(std.heap.wasm_allocator);
    defer headers.deinit();

    return handler(req, &headers) catch |err| {
        std.log.err(
            "Error: {any}\nName: {s}\nZig Version: {s}\n",
            .{ err, @errorName(err), builtin.zig_version_string },
        );
        headers.clearRetainingCapacity();
        headers.append("Content-Type", "text/html") catch unreachable;
        const body = "<h1>Internal Server Error</h1>";
        return .{
            .body = std.ArrayListUnmanaged(u8).initBuffer(@constCast(body)),
            .headers = headers,
            .status = .internal_server_error,
        };
    };
}

fn handler(req: spin.http.Request, headers: *std.http.Headers) !spin.http.Response {
    const uri = try std.Uri.parse(req.url);
    std.log.err("uri: {any}\n", .{uri});

    const stdin = std.io.getStdIn();
    const a = try stdin.reader().readAllAlloc(std.heap.wasm_allocator, std.math.maxInt(usize));
    _ = a;
    var res: spin.http.Response = .{
        .body = try std.ArrayListUnmanaged(u8).initCapacity(
            std.heap.wasm_allocator,
            BODY_CAP,
        ),
        .headers = headers.*,
    };

    try Router.route(uri, &res);
    // try buf_writer.flush();

    return res;
}

pub fn main() void {
    spin.http.HANDLER = &error_handler;
}
