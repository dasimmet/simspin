<svg xmlns="http://www.w3.org/2000/svg" width="120" height="20">
  <linearGradient id="b" x2="0" y2="100%">
    <stop offset="0" stop-color="#bbb" stop-opacity=".1" />
    <stop offset="1" stop-opacity=".1" />
  </linearGradient>

  <mask id="a">
    <rect width="120" height="20" rx="3" fill="#fff" />
  </mask>

  <g mask="url(#a)">
    <path fill="{bg_left}" d="M0 0 h62 v20 H0 z" />
    <path fill="{bg_right}" d="M62 0 h58 v20 H62 z" />
    <path fill="url(#b)" d="M0 0 h120 v20 H0 z" />
  </g>

  <g fill="#fff" text-anchor="middle">
    <g font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="11">
      <text x="31" y="15" fill="{fg_col}" fill-opacity=".3">
        {left}
      </text>
      <text x="31" y="14">
        {left}
      </text>
      <text x="91" y="15" fill="{fg_col}" fill-opacity=".3">
        {right}
      </text>
      <text x="91" y="14">
        {right}
      </text>
    </g>
  </g>
</svg>