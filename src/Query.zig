const std = @import("std");

const QueryEntry = struct {
    key: []const u8,
    value: []const u8,
};
const StringStringList = std.MultiArrayList(QueryEntry);

pub fn parse(allocator: std.mem.Allocator, query: []const u8) !*StringStringList {
    var map = StringStringList{};
    var at_key: bool = true;
    var acc: []const u8 = query;
    acc.len = 0;
    var key: []const u8 = undefined;
    for (query, 0..) |c, i| {
        acc.len += 1;
        if (at_key and c == '=') {
            at_key = false;
            key = acc[0 .. acc.len - 1];
            acc.ptr = @ptrFromInt(@intFromPtr(query.ptr) + i + 1);
            acc.len = 0;
        }
        if (!at_key and (c == '&' or i == query.len)) {
            const un_key = try std.Uri.unescapeString(allocator, key);
            // defer allocator.free(un_key);
            const un_val = try std.Uri.unescapeString(allocator, acc[0 .. acc.len - 1]);
            // defer allocator.free(un_val);

            try map.append(allocator, .{
                .key = un_key,
                .value = un_val,
            });
            acc.ptr = @ptrFromInt(@intFromPtr(query.ptr) + i + 1);
            acc.len = 0;
            at_key = true;
        }
    }
    if (!at_key) {
        const un_key = try std.Uri.unescapeString(allocator, key);
        // defer allocator.free(un_key);
        const un_val = try std.Uri.unescapeString(allocator, acc[0..acc.len]);
        // defer allocator.free(un_val);

        try map.append(allocator, .{
            .key = un_key,
            .value = un_val,
        });
    }
    return &map;
}
