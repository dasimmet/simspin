const std = @import("std");
const spin = @import("spin");
const ZBuild = @import("ZBuild").ZBuild;
pub const Query = @import("Query.zig");
pub const PATH = "/badge.svg";

const Template = @embedFile("badge.svg.tpl");
const TemplateValues = struct {
    left: []const u8 = "coverage",
    right: []const u8 = "100%",
    fg_col: []const u8 = "#01f101",
    bg_left: []const u8 = "#555",
    bg_right: []const u8 = "#9f9f9f",
};

pub fn render(allocator: std.mem.Allocator, uri: std.Uri, writer: anytype) !void {
    var vals = TemplateValues{};
    if (uri.query) |qu| {
        const q = try Query.parse(
            allocator,
            qu,
        );
        defer q.deinit(allocator);

        for (0..q.len) |i| {
            const it = q.get(i);
            std.log.err(
                "parsed_query: key: {s} value: {s}",
                .{ it.key, it.value },
            );

            inline for (@typeInfo(@TypeOf(vals)).Struct.fields) |f| {
                if (f.type == @TypeOf(it.value) and std.mem.eql(u8, f.name, it.key)) {
                    @field(vals, f.name) = it.value;
                }
            }
        }
    }
    try ZBuild.fmt.format(
        Template,
        writer,
        vals,
    );
}
