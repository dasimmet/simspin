const std = @import("std");
const spin = @import("spin");

pub const Router = @This();

pub const Badge = @import("Badge.zig");
pub const Tab = @import("Tab.zig");

pub const Route = struct {
    paths: []const []const u8,
    mimetype: []const u8,
    static: ?[]const u8 = null,
};

const RouteArray = [_]Route{.{
    .mimetype = "text/html",
    .paths = &.{
        "",
        "/",
        "index.html",
    },
    .static = @embedFile("ugtar.html"),
}};

pub fn route(uri: std.Uri, res: *spin.http.Response) !void {
    const writer = res.body.writer(std.heap.wasm_allocator);

    for (RouteArray) |r| {
        for (r.paths) |p| {
            if (p.len == uri.path.len and std.mem.eql(u8, uri.path[0..p.len], p)) {
                if (r.static) |s| {
                    try res.headers.append("Content-Type", r.mimetype);
                    _ = try writer.writeAll(s);
                    _ = try writer.writeAll("\x00");
                    return;
                }
            }
        }
    }
    if (std.mem.eql(u8, uri.path[0..Badge.PATH.len], Badge.PATH)) {
        try res.headers.append("Content-Type", "image/svg+xml; charset=utf-8");
        try Badge.render(std.heap.wasm_allocator, uri, writer);
    } else if (std.mem.eql(u8, uri.path[0..Tab.PATH.len], Tab.PATH)) {
        try res.headers.append("Content-Type", "text/html");
        try Tab.render(std.heap.wasm_allocator, uri, writer);
    } else {
        try res.headers.append("Content-Type", "text/html");
        res.status = .not_found;
        _ = try writer.writeAll("<h1>404 - Not Found</h1>");
    }
}
