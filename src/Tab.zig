const std = @import("std");
const spin = @import("spin");

pub const PATH = "/tab/";

pub fn render(allocator: std.mem.Allocator, uri: std.Uri, writer: anytype) !void {
    _ = writer;
    const path = uri.path[PATH.len..];
    const urlpath = try std.Uri.unescapeString(allocator, path);
    defer allocator.free(urlpath);

    const req1 = spin.http.Request{ .method = .GET, .url = urlpath };
    std.log.err("req1: {any}\nurlpath: {s}", .{ req1, urlpath });
    const res1 = try spin.http.send(req1);
    std.log.err("res1: {any}", .{res1});
    std.log.err("res1: {s}", .{res1.body.items});
}
