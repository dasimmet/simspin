const std = @import("std");
const ZBuild: type = @import("ZBuild").ZBuild;

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{
        .default_target = .{
            .cpu_arch = .wasm32,
            .os_tag = .wasi,
        },
    });
    const optimize = b.option(
        std.builtin.OptimizeMode,
        "optimize",
        "set optimize mode",
    ) orelse .ReleaseSafe;

    _ = ZBuild.init(.{
        .owner = b,
    });
    const spin_exe = spinExe(b, target, optimize);

    var spin_step = b.step("spin", "create the spin binary");
    const spin_install = b.addInstallArtifact(spin_exe, .{});

    spin_step.dependOn(&spin_install.step);

    const install = b.getInstallStep();
    install.dependOn(spin_step);

    const run_cmd = b.addSystemCommand(&[_][]const u8{
        "spin",
        "build",
        "--up",
    });
    const run = b.step("run", "run spin");
    run.dependOn(&run_cmd.step);

    b.step("fmt", "Format Code").dependOn(&b.addFmt(.{
        .paths = &[_][]const u8{
            "src",
            "build.zig",
            "build.zig.zon",
        },
    }).step);

    const spin_docs_exe = spinExe(b, target, optimize);

    const docs_step = b.step("docs", "Emit docs");
    const docs_install = b.addInstallDirectory(.{
        .source_dir = spin_docs_exe.getEmittedDocs(),
        .install_dir = .prefix,
        .install_subdir = "docs",
    });
    docs_step.dependOn(&docs_install.step);
    b.default_step.dependOn(docs_step);
}

fn spinExe(
    b: *std.Build,
    target: std.Build.ResolvedTarget,
    optimize: std.builtin.OptimizeMode,
) *std.Build.Step.Compile {
    const spin = b.dependency("spin", .{});

    const spin_exe = b.addExecutable(.{
        .name = "spin",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });
    linkSpin(spin_exe, spin);
    spin_exe.root_module.addImport("ZBuild", b.dependency(
        "ZBuild",
        .{},
    ).module("ZBuild"));
    spin_exe.root_module.addImport("rem", b.dependency(
        "rem",
        .{},
    ).module("rem"));

    return spin_exe;
}

fn linkSpin(exe: *std.Build.Step.Compile, spin: *std.Build.Dependency) void {
    inline for (&[_][]const u8{
        "sqlite",
        "wasi-outbound-http",
        "spin-http",
        "spin-config",
    }) |f| {
        exe.addCSourceFile(.{
            .file = spin.path("include/" ++ f ++ ".c"),
            .flags = &.{
                "-Wno-unused-parameter",
                "-Wno-switch-bool",
            },
        });
    }

    const mod = spin.module("spin");
    mod.addIncludePath(spin.path("include"));

    exe.root_module.addImport("spin", mod);
    exe.linkLibrary(spin.artifact("spin"));
    exe.step.dependOn(spin.builder.default_step);
}
